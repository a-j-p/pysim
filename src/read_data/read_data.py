import os
from pathlib import Path
import pandas as pd

# Add paths to get .ROOT points to ../PYSIM
ROOT = Path(__file__).parent.parent.parent
resources = os.path.join(ROOT, 'resources')
cars1_dta = "cars1.dta"
cars1_csv = "cars1.csv"


# Converts cars.dta file to cars.csv and uploads it to resources
# Needs only to be executed the first time
def dta_to_csv(execute=False):
    if execute:
        absolute_path_dta = os.path.join(resources, cars1_csv)
        data = pd.io.stata.read_stata(absolute_path_dta)
        data.to_csv(os.path.join(resources, cars1_csv))


# Read cars.csv into pandas data frame
def create_df():
    df = pd.read_csv(os.path.join(resources, cars1_csv))
    return df
