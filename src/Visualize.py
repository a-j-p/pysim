from src.Models.Merger import Merger
from time import time
from bokeh.models import ColumnDataSource, Button, Select, Div, HoverTool
from bokeh.plotting import figure
from bokeh.layouts import column, row
from math import pi
from bokeh.server.server import Server
from src.read_data.read_data import create_df
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

start = time()


"""
Name:       visualize
Purpose:    Runs the interactive visualization of the results on local bokeh server
Input:      curdoc, file figures are added to as layouts
Output:     HTML file with bokeh interface

Local Functions: 
            callback_1: Callback for year, updates options for selection widget of firm1 and firm2
            callback_2: Callback for country
            callback_3: Callback for firm 1, updates options for selection widget of firm2
            callback_4: Callback for firm 2.
            callback_5: To execute merger
"""


def visualize(curdoc):
    df_opt = create_df()

    # for merger initially displayed and lookup for current values selected
    dct = {'firm_1': 'GM', 'firm_2': 'VW', 'year': 1998, 'country': 'Belgium'}

    def callback_1(attr, old, new):
        # update year from dropdown and restrict firm options
        dct['year'] = int(new)
        options_3_new = sorted(set(df_opt.loc[df_opt['year'] == int(new)]['firm']))
        select_3.options = options_3_new
        select_4.options = options_3_new

    def callback_2(attr, old, new):
        # update country from dropdown
        dct['country'] = new

    def callback_3(attr, old, new):
        # update firm 1 from dropdown
        dct['firm_1'] = new

        # update options of second drop down
        options_4_new = sorted(select_3.options)
        options_4_new.remove(new)
        select_4.options = options_4_new

    def callback_4(attr, old, new):
        # update firm 2 from dropdown
        dct['firm_2'] = new

    def callback_5():

        # update source based on modified values from other callbacks stored in dct
        df, ps, df_circle = Merger(dct['year'], dct['country'], dct['firm_1'], dct['firm_2'])
        df = df.sort_values(by=['p_dif'], ascending=False)
        df['p_dif'] = df['p_dif'] * 1000
        df['color'] = df_circle.at[0, 'color']
        source.data = df.head(20)

        # update circle glyph in f_2
        source_circle.data = df_circle

        # update x axis range of f_1
        f_1.x_range.factors = list(source.data['type'])

        # update title of figure f_1
        f_1.title.text = f"Price Change Of {dct['firm_1']}/{dct['firm_2']} Merger In {dct['year']}"

        # update producer surplus in div
        if ps <= 0:
            div.text = 'Producer Surplus is approximately 0'
        else:
            div.text = f'Producer Surplus: {ps:,d} $'

    # get dataframe from merger
    df, ps, df_circle = Merger(dct['year'], dct['country'], dct['firm_1'], dct['firm_2'])

    # sort by price difference, select top 10 and create column datasource
    df = df.sort_values(by=['p_dif'], ascending=False)
    df['p_dif'] = df['p_dif']*1000
    df['color'] = '#003366'
    source = ColumnDataSource(df.head(20))

    # create column datasource for circle plot
    source_circle = ColumnDataSource(df_circle)

    # create figures
    f_1 = figure(plot_width=1300, title=f"Price Change Of {dct['firm_1']}/{dct['firm_2']} Merger In {dct['year']}",
                 x_range=source.data['type'], x_axis_label='Car Model', y_axis_label='Price Change In $')

    f_2 = figure(plot_width=350, plot_height=400, title='Average Price change')

    # add vertical bars to figure 1 (f_1)
    f_1.vbar(source=source, x='type', top='p_dif', width=0.5, color='color')

    # options for drop down widgets
    options_1 = list(sorted(set(df_opt['year'].apply(str))))
    options_2 = list(sorted(set(df_opt['country'].apply(str))))
    options_3 = list(sorted(set(df['firm'])))
    options_4 = list(sorted(set(df['firm'])))
    options_4.remove(dct['firm_1'])

    # add buttons
    select_1 = Select(title='Year', value='1998', options=options_1)
    select_2 = Select(title='Country', value='Belgium', options=options_2)
    select_3 = Select(title='Firm 1', value=dct['firm_1'], options=options_3)
    select_4 = Select(title='Firm 2', value=dct['firm_2'], options=options_4)
    button = Button(label='Merge')

    # hook buttons on callback functions
    select_1.on_change('value', callback_1)
    select_2.on_change('value', callback_2)
    select_3.on_change('value', callback_3)
    select_4.on_change('value', callback_4)
    button.on_click(callback_5)

    # plot 1 appearance
    f_1.xgrid.visible = False
    f_1.ygrid.visible = False
    f_1.xaxis.major_label_orientation = pi/3

    # plot 2 appearance
    f_2.axis.visible = False
    f_2.toolbar.logo = None
    f_2.toolbar_location = None
    f_2.xgrid.visible = False
    f_2.ygrid.visible = False
    f_2.outline_line_color = None

    # add hover tool to bar plot
    hover_1 = HoverTool(mode='vline')
    hover_1.tooltips = [("Firm", "@{firm}"), ("Pre-Merger Average Price", "@{price}"),
                        ("Post-Merger Average Price", "@{p_post}")]
    f_1.add_tools(hover_1)

    # add text below merger
    div = Div(text=f'Producer Surplus: {ps:,d} $')

    # add circle glyphs to f_2
    f_2.circle(x=0, y=250, size=200, alpha=0.5, line_alpha=0, color='color', source=source_circle,
               legend_field='circle_1')
    f_2.circle(x=0, y=250, size='frac', alpha=0.8, line_alpha=0, color='color', source=source_circle,
               legend_field='circle_2')
    f_2.legend.location = "bottom_center"

    # print running time to calculate merger
    end = time()
    print(f"\n\nrunning time: {end - start}")

    # create layout and add it to curdoc
    layout = row(column(select_1, select_2, select_3, select_4, button, div, f_2), f_1)
    curdoc.add_root(layout)


# enables to run bokeh server as standalone
server = Server({'/': visualize}, num_procs=1)
server.start()
server.io_loop.add_callback(server.show, "/")
server.io_loop.start()
