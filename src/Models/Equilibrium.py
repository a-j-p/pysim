from src.read_data.read_data import resources
from src.Models.Supply import supply
import pandas as pd
from scipy.optimize import newton
import numpy
from scipy import sparse
import warnings

# enables full display of data. Not imperative for execution
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('expand_frame_repr', False)

"""
Name:    Equilibrium
Purpose: Calculates the the post merger Bertrand Nash equilibrium
Input:   year of the merger, alpha, sigma_0 and sigma_1 as calculated in demand
Output:  Post merger prices to calculate welfare loss
"""


def equilibrium(year, country, firm_1, firm_2, alpha, sigma_0, sigma_1):
    # lookup for country
    dct = {'Belgium': 0, 'France': 1, 'Germany': 2, 'Italy': 3, 'UK': 4}

    # call supply to get post merger omega matrices
    omegas = supply(year, firm_1, firm_2, alpha, sigma_0, sigma_1)
    df = pd.read_csv(resources + '//df.csv', sep=';')

    # reduce dataframe to Belgium and year == year
    df = df.loc[df['year'] == year]
    df = df.loc[df['country'] == country]
    df_ = df.copy(deep=True)

    # get inverted post merger omega for country
    omega_post = omegas[dct[country]][1]
    omega_post = sparse.csc_matrix.toarray(omega_post)
    omega_post_inv = numpy.linalg.pinv(omega_post)

    # calculate equilibrium using newton-raphson root finding algorithm
    r = newton(lambda s: formulation(s, alpha, sigma_0, sigma_1, omega_post_inv, df_), x0=df['qu'], maxiter=3,
               tol=10, full_output=True)

    # get percentage converged
    df['q_post'] = r.root
    converged = list(r.converged).count(True)
    convergence_ratio = round(converged/len(df['q_post']), 2)
    print('Convergence Ratio: ', convergence_ratio, '%\n')

    # raise warning if convergence ratio is bad
    if convergence_ratio < 0.8:
        warnings.warn("There might be no equilibrium")

    df['q_post'] = df['q_post'].fillna(df['qu'])

    # create new dataframe to display pre and post merger average quantities and prices
    df['q_pre'] = df['qu']

    df['lerner'] = (df['price']-df['c'])/df['price']
    lerner = df.groupby('firm')['lerner'].mean()

    q_pre = df.groupby('firm')['q_pre'].sum()
    q_post = df.groupby('firm')['q_post'].sum()

    d = pd.merge(q_pre, q_post, how='outer', on='firm')

    d['q_dif'] = d['q_post']-d['q_pre']
    df['p_post'] = get_prices(omega_post_inv, df)
    df['p_dif'] = df['p_post']-df['price']

    # raise Warning if equilibrium condition is not fulfilled
    a = list(-alpha * (df['price']-df['c']))

    # add price information to new dataframe
    d['p_pre'] = df.groupby('firm')['price'].mean()
    d['p_post'] = df.groupby('firm')['p_post'].mean()
    d['p_dif'] = round(d['p_post']-d['p_pre'], 5)
    d['lerner'] = lerner

    print(d)

    # calculate price information for circle plot
    p_dif_1 = round(d.loc[firm_1, 'p_dif']*1000, 2)
    p_dif_2 = round(d.loc[firm_2, 'p_dif']*1000, 2)
    if p_dif_1 < p_dif_2:
        frac = p_dif_1/p_dif_2
        larger_firm = firm_2
        larger_price = p_dif_2
        smaller_firm = firm_1
        smaller_price = p_dif_1
    else:
        frac = p_dif_2/p_dif_1
        larger_firm = firm_1
        larger_price = p_dif_1
        smaller_firm = firm_2
        smaller_price = p_dif_2

    color = {'Belgium': '#003366', 'France': "slategrey", 'Germany': "goldenrod", 'Italy': "#00501B",
             'UK': "darkslategray"}

    df_circle = pd.DataFrame({'frac': [200 * frac], 'color': color[country], 'p_dif_1': [p_dif_1], 'p_dif_2': [p_dif_2],
                              'circle_1': [f'Average Price Change of {larger_firm}: {larger_price}$'],
                              'circle_2': [f'Average Price Change of {smaller_firm}: {smaller_price}$']})
    # get producer surplus ps
    ps = get_profit_dif(df)
    return df, int(ps), df_circle


"""
Name:    Formulation
Purpose: Provides the formulation of the equilibrium first order condition used in the root finding algorithm
Input:   Quantities Q, alpha, sigma_0, sigma_1, inverted post merger omega and pointer to dataframe
Output:  Equilibrium first order condition as a function of prices Quantity vector
"""


def formulation(s, alpha, sigma_0, sigma_1, omega_post_inv, df):
    # log(s_j/s_0)
    df['f_qu'] = s
    df['f_s_j'] = s / df['pop']
    df['f_s_0'] = 1-df['f_s_j'].sum()
    df['f_s_jt_l'] = numpy.log(df['f_s_j']/df['f_s_0'])

    # sigma_0 * log(s_j|hg)
    q_h = df.groupby(['segment', 'domestic'])['f_qu'].sum()
    q_h = q_h.rename('f_q_h')
    df = pd.merge(df, q_h, how='outer', on=['segment', 'domestic'])
    df['f_s_jh_l'] = numpy.log(df['f_qu']/df['f_q_h'])

    # sigma_1 * log(s_h|gt)
    q_g = df.groupby(['segment'])['f_qu'].sum()
    q_g = q_g.rename('f_q_g')
    df = pd.merge(df, q_g, how='outer', on=['segment'])
    df['f_s_hg_l'] = numpy.log(df['f_q_h']/df['f_q_g'])
    df = df.sort_values(by=['co'])

    # delta - alpha * p
    df['f_delta'] = df['delta']

    # subtract supply from demand to get objective equation and return it
    df['f_p'] = (df['f_s_jt_l'] - sigma_0 * df['f_s_jh_l'] - sigma_1 * df['f_s_hg_l'] - df['f_delta'])/alpha

    return df['f_p']-df['c']-omega_post_inv.dot(df['qu'])


"""
Name:     get_prices
Purpose:  Calculate post merger prices based on post merger quantities
Input:    Inverted post merger omega and pointer to dataframe
Output:   Post merger price vector
"""


def get_prices(omega_post_inv, df):
    return df['c'] + omega_post_inv.dot(df['q_post'])


"""
Name:    get_profit_dif
Purpose: Calculate producer surplus gain due to the merger
Input:   Pointer to dataframe
Output:  Profit difference between post- and pre merger
"""


def get_profit_dif(df):
    # calculate pre and post merger producer surplus
    profit_pre = (df['price']*1000-df['c']*1000).dot(df['qu'])
    profit_post = (df['p_post']*1000-df['c']*1000).dot(df['q_post'])

    # calculate producer surplus counterfactual
    profit_dif = profit_post-profit_pre
    return profit_dif
