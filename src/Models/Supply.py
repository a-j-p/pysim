import pandas as pd
from scipy import sparse
import numpy
from itertools import product
from src.read_data.read_data import resources

# enables full display of data. Not imperative for execution
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('expand_frame_repr', False)

"""
Name:    Supply 
Purpose: calculation of the marginal costs and the post merger omega matrices required to calculate the post merger
         equilibrium
Input:   year of the merger, alpha, sigma_0 and sigma_1 as estimated in demand, firm1 and firm2: firms to be merger
output:  omegas: list containing the omega matrix of each country
         mc: marginal cost are added to df.csv      
"""


def supply(year, firm1, firm2, alpha, sigma_0, sigma_1):

    # get dataframe prepared in demand
    df = pd.read_csv(resources + '//df.csv', sep=';',)

    # get dataframe only for year==year
    df = df.loc[df['year'] == year]

    # initialize an instance of the elasticity class to calculate and store the elasticities
    ela = Elasticities(alpha, sigma_0, sigma_1)

    # create list holding the omega matrices for each country
    omegas = []
    for country in ['Belgium', 'France', 'Germany', 'Italy', 'UK']:
        omegas.append((country, create_omega(country, df, ela)))
    ela.get_means()
    marginal_cost(omegas, df)

    # return omegas
    # change the ownership structure by setting the name of firm1 to firm2
    df['firm'] = df['firm'].replace([firm1], firm2).copy(deep=True)

    # create list holding the post merger omega matrices of each country
    omegas_post = []
    for country in ['Belgium', 'France', 'Germany', 'Italy', 'UK']:
        omegas_post.append((country, create_omega(country, df, ela)))

    return omegas_post


"""
Name:     marginal_cost
Purpose:  calculate marginal costs
Input:    omega matrix of country x
Output:   marginal cost vector

-> MarginalCosts is a helper class for the function to store and represent the calculated costs
"""


class MarginalCosts:
    def __init__(self, df):
        self.companies = dict.fromkeys(set(df['firm']))
        self.df = df

    def get_means(self):
        print(15*'_'+'marginal costs'+15*'_'+'\n')
        for firm in set(self.df['firm']):
            print(f"mean  {firm:<29}{round(self.df.loc[self.df['firm'] == firm]['c'].mean(),4)}")
        print(45*'_'+'\n\n')


def marginal_cost(omegas, df):

    # declare series holding marginal costs of each product j
    c = pd.Series(dtype=float)

    # calculate marginal costs based on each country's omega matrix
    for country, o in omegas:
        omega = sparse.csc_matrix.toarray(o)

        # invert the omega. Use pseudo inverse as theta is singular.
        omega_inverted = numpy.linalg.pinv(omega)

        # create vector with the marginal cost of each product j in the dataframe
        df_p = df.loc[df['country'] == country]
        c = c.append(df_p['price'] - numpy.dot(omega_inverted, df_p['qu']))

    # overwrite the marginal costs in the dataframe and upload it to resources
    c = c.rename('c')
    df['c'] = c

    df.to_csv(resources+'/df.csv', sep=';', index=False)
    # print mean marginal costs for each firm
    mc = MarginalCosts(df)
    mc.get_means()
    return


"""
Name:    creat_omega
Purpose: Calculate pre- and post merger omega matrices
Input:   Country, pointer to dataframe and instance of Elasticities
Output:  Omega matrix either based on the old or new ownership structure
"""


def create_omega(country, df, ela):
    df = df.loc[df['country'] == country]

    # Factorize products to use them as indices in omega.
    keys, vals = pd.factorize(df['co'].copy())
    df = df.assign(co=keys)

    # create dictionary with firm as key and factorized products j as value
    dct = dict((key, set(df.loc[df['firm'] == key]['co'])) for key in set(df['firm']))

    # create a binary matrix theta from dct by taking cartesian product of each firm's list of products j
    lst_coordinates = []
    for key in dct.keys():
        lst_coordinates.extend(list(product(dct[f'{key}'], repeat=2)))
    lst = list(zip(*lst_coordinates))

    x_idx = lst[0]
    y_idx = lst[1]

    # list containing dq/dp, that is the non zero values of matrix omega
    data = []

    # get index of columns holding segment and domestic
    segment = df.columns.get_loc('segment')
    domestic = df.columns.get_loc('domestic')

    # calculates dq/dp by calling the functions provided by the elasticity class
    for x, y in lst_coordinates:
        # get segment and domestic dummy to determine weather the two products are in the same group and subgroup
        x_segment = df.iat[x, segment]
        y_segment = df.iat[y, segment]
        x_domestic = df.iat[x, domestic]
        y_domestic = df.iat[y, domestic]

        # products are the same
        if x == y:
            data.append(ela.elasticity_ejj(x, df))

        # products are in the same subgroup
        elif x_segment == y_segment and x_domestic == y_domestic:
            data.append(ela.elasticity_ejk_h(x, df))

        # products are in the same group
        elif x_segment == y_segment:
            data.append(ela.elasticity_ejk_g(x, df))

        # products are not in the same group
        else:
            data.append(ela.elasticity_ejk(x, df))

    # create omega based on the dq/dp computed above
    omega = sparse.csc_matrix((data, (x_idx, y_idx)))
    return omega


"""
Name:    elasticities
Purpose: Bundle all function to calculate elasticities and keep track of their means.
Provides Methods:
    elasticity_ejj
    elasticity_ejk_h
    elasticity_ejk_g
    elasticity_ejk
    get_means     

Class variables:
    e_jj:   sum of own price elasticities
    e_jk_h: sum of elasticities of products in the same subgroup
    e_jk_g: sum of elasticities of products in the same group but not subgroup
    e_jk:   sum of elasticities of products in different groups

-> only the elasticities required are calculated
"""


class Elasticities:
    def __init__(self, alpha, sigma_0, sigma_1):
        self.e_jj = 0
        self.e_jk_h = 0
        self.e_jk_g = 0
        self.e_jk = 0

        self.e_jj_count = 1
        self.e_jk_h_count = 1
        self.e_jk_g_count = 1
        self.e_jk_count = 1

        self.alpha = alpha
        self.sigma_0 = sigma_0
        self.sigma_1 = sigma_1

    def elasticity_ejj(self, idx1, df):

        # extract values required to calculate own price elasticity
        q_j, q_h, q_g, p, pop = df.iloc[idx1][['qu', 's_ht', 's_gt', 'price', 'pop']]

        # calculate own price elasticity based on formula in Verboven 1996
        t = self.alpha * (1/(1-self.sigma_0) - (1/(1-self.sigma_0) - 1/(1-self.sigma_1)) * q_j/q_h -
                          self.sigma_1/(1-self.sigma_1) * q_j/q_g - q_j/pop)
        e_jj = t*p
        s_jj = t*q_j
        self.e_jj += e_jj
        self.e_jj_count += 1
        return -s_jj

    def elasticity_ejk_h(self, idx1, df):

        # extract values required to calculate cross price elasticities for goods in same subgroup
        q_j, q_h, q_g, p, pop = df.iloc[idx1][['qu', 's_ht', 's_gt', 'price', 'pop']]

        # calculate cross price elasticity e_jk_h
        t = self.alpha * ((1/(1-self.sigma_0)-1/(1-self.sigma_1)) * q_j/q_h + (self.sigma_1/(1-self.sigma_1)) * q_j/q_g
                          + q_j/pop)
        s_jk_h = -t * q_j
        e_jk_h = -t * p
        self.e_jk_h += e_jk_h
        self.e_jk_h_count += 1
        return -s_jk_h

    def elasticity_ejk_g(self, idx1, df):

        # extract values required to calculate cross price elasticities for goods in same group (but not subgroup)
        q_j, q_g, p, pop = df.iloc[idx1][['qu', 's_gt', 'price', 'pop']]

        # calculate cross price elasticity e_jk_g
        t = self.alpha * ((1/1-self.sigma_1) * q_j/q_g + q_j/pop)

        e_jk_g = -t*p
        s_jk_g = -t*q_j
        self.e_jk_g += e_jk_g
        self.e_jk_g_count += 1
        return -s_jk_g

    def elasticity_ejk(self, idx1, df):

        # extract values required to calculate cross price elasticities for goods in different groups
        q_j, p, pop = df.iloc[idx1][['qu', 'price', 'pop']]

        # calculate cross price elasticity e_jk
        t = self.alpha * (q_j/pop)
        s_jk = -t*q_j
        e_jk = -t*p
        self.e_jk += e_jk
        self.e_jk_count += 1
        return -s_jk

    # prints the mean of all elasticities calculated
    def get_means(self):
        print(12*'_'+'elasticties'+11*'_')
        print(f"mean e_jj:  \t\t\t{round(self.e_jj/self.e_jj_count, 6)}")
        print(f"mean e_jk_h:\t\t\t{round(self.e_jk_h/self.e_jk_h_count, 6)}")
        print(f'mean e_jk_g:\t\t\t{round(self.e_jk_g/self.e_jk_g_count, 6)}')
        print(f'mean e_jk:  \t\t\t{round(self.e_jk/self.e_jk_count, 6)}')
        print('__________________________________\n\n')
        return
