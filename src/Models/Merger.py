from src.Models.Demand import demand
from src.Models.Equilibrium import equilibrium

"""
Name:    Merger
Purpose: Intermediate function for Visualize
Input:   Year, Country and the two firms to be merged
Output:  Dataframe df containing the merger results, Dataframe df_circle containing information fro circle plot
"""


def Merger(year, country, firm_1, firm_2):

    # get coefficients from demand
    alpha, sigma_0, sigma_1 = demand()

    # get dataframe containing results of the merger, producer surplus, and data for circle plot
    df, ps, df_circle = equilibrium(year, country, firm_1, firm_2, alpha, sigma_0, sigma_1)
    return df, ps, df_circle

# Merger(1998, 'Belgium', 'GM', 'VW')
