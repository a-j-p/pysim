import pandas as pd
from src.read_data.read_data import create_df, resources
from linearmodels import PanelOLS
import statsmodels.api as sm
import math

"""
Name:    Demand 
Purpose: Estimation of demand function
Input:   None
Output:  Estimated parameters alpha, sigma_0 and sigma_1

Local variables:
    s_t:   sales of all products in year & country t (f.i. total car sales in 1993 in Belgum) 
    s_gt:  sales of all products in subgroup h in year & country t 
    s_ht:  sales of all products in group g in year & country t
    
    s_0:   share of the outside good (difference between pop/4 and s_t)
    s_jt:  share of product j in year & country t, relative to outside good s_0 share
    s_jht: share of products j in year & country t, relative to subgroup s_ht       
    s_ght: share of subgroup h in year & country t, relative to group s_gt
    
    -> subscript _l means natural log was taken
"""


def demand():

    # enables full display of data. Not imperative for execution
    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_rows', None)
    pd.set_option('expand_frame_repr', False)

    # get dataframe created in read_data.py
    df = create_df()

    # create variable year_ to be able to use year as a regressor. Not possible with year as index.
    df['year_'] = df['year']

    # calculate total sales for each (year, country) and add it to the dataframe as 's_t'
    s_t = df.groupby(['year', 'country'])['qu'].sum().reset_index(drop=False)
    s_t = s_t.rename(columns={'qu': 's_t'})
    df = pd.merge(df, s_t, how="outer", on=['year', 'country'])

    # define potential market as population divided by four. Crude proxy for number of households
    df['pop'] = df['pop']/4

    # calculate outside good share as the difference between potential market and total number of cars bought and
    # divide by market size.
    df['s_0'] = (df['pop']-df['s_t'])/df['pop']

    # calculate total market share of product j and take natural log to get dependent variable s_jt_l
    df['s_jt'] = df['qu']/df['pop']
    df['s_jt_0'] = df['s_jt']/df['s_0']
    df['s_jt_l'] = df['s_jt_0'].apply(lambda x: math.log(x))

    # calculate total sales for each subgroup (i.e. segment & domestic) in a given (year, country) and add it to the
    # dataframe as 's_ht'
    df_s_gt = df.groupby(['year', 'country', 'segment', 'domestic'])['qu'].sum().reset_index(drop=False)
    df_s_gt = df_s_gt.rename(columns={'qu': 's_ht'})
    df = pd.merge(df, df_s_gt, how="outer", on=['year', 'country', 'segment', 'domestic'])

    # calculate share of product j in subgroup h and take natural log to get independent variable s_jht_l
    df['s_jht'] = df['qu']/df['s_ht']
    df['s_jht_l'] = df['s_jht'].apply(lambda x: math.log(x))

    # calculate sales of group in a given year & country and add it to the dataframe as 's_gt'
    df_s_ht = df.groupby(['year', 'country', 'segment'])['qu'].sum().reset_index(drop=False)
    df_s_ht = df_s_ht.rename(columns={'qu': 's_gt'})
    df = pd.merge(df, df_s_ht, how="outer", on=['year', 'country', 'segment'])

    # calculate share of subgroup h in group g and take natural log to get independent variable s_ght
    df['s_hgt'] = df['s_ht']/df['s_gt']
    df['s_hgt_l'] = df['s_hgt'].apply(lambda x: math.log(x))
    df = df.set_index(['co', 'year']).sort_index()
    df.rename(columns={'Unnamed: 0': 'ID'}, inplace=True)

    # estimate log-lin panel regression to get parameters alpha sigma_0 and sigma_1
    exog_var = ['s_jht_l', 's_hgt_l', 'price', 'fuel', 'width', 'height', 'domestic', 'horsepower', 'country2',
                'country3', 'country4', 'country5', 'year_']

    exo = sm.add_constant(df[exog_var])
    mod = PanelOLS(df['s_jt_l'], exo, entity_effects=True)
    fit = mod.fit()

    # retrieve parameters used for supply side optimization problem
    alpha = fit.params['price']
    sigma_0 = fit.params['s_jht_l']
    sigma_1 = fit.params['s_hgt_l']

    # get mean valuations for equilibrium calculation.
    df['delta'] = df['s_jt_l']-sigma_0*df['s_jht_l']-sigma_1*df['s_hgt_l']-alpha*df['price']

    print(f"alpha: {round(alpha, 5)}")
    print(f"sigma_0: {round(sigma_0, 5)}")
    print(f"sigma_1: {round(sigma_1, 5)}\n\n")
    print(fit.summary)

    # store dataframe in resources
    df.to_csv(resources+'/df.csv', sep=';')
    return alpha, sigma_0, sigma_1
