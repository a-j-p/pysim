"""
########################################################################################################################
Name:       PYSIM
Type:       Package
Purpose:    Program to calculate horizontal merger simulations based on car_1 data set by Golberg and Verboven 2001
Provides:
            Read_data: Loads data from resource folder and provides interface for further data cleansing
            Demand: Estimation of two level nested logit demand
            Supply: Calculates elasticities, pre- and post merger omega matrices and marginal costs
            Equilibrium: Calculates the counterfactual prices, market shares and producer surplus
            Merger: Interface for further efficiency improvements and error handling
            Visualize: Provides bokeh application for interactive visualization of the results

Date:        26.2.2021
Institution: UZH faculty of economics
Author:      Andrin Pluess
########################################################################################################################
"""
