The main functions for the simulation can be found in src.Models.

To run the full programm please execute the visualize file, which starts the external bokeh application. 

The simulation is designed to be run as a standalone i.e. the local bokeh server can be started from the IDE directly by running the visualize file and does not need to be invoked from the command line.

Each of the modules demand, supply and equilibrium can be executed seperately by invoking the function in the corresponging files. 

To calculate the whole simulation without starting the bokeh application, uncomment the function Merger in the file in src.Models.Merger. This calls demand first which callculates alpha, sigma_0 and sigma_1 required for the estimation of the marginal costs. Second the equilibrium function is executed which calls supply to get the omega matrices and marginal costs.

The preprocessed dataframe is stored in resources.

Please note the dependencies.

-> The package has been tested on a Windows computer in a PyCharme 2019 1.1 IDE. 
